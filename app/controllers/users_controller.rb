class UsersController < ApplicationController
  def new
    render :new
  end

  def create
    @user = User.new(params[:user])
    if @user.save
      flash[:notice] = "You successfully created a User!!!!!"
      session[:session_token] = @user.session_token
      redirect_to user_url(@user)
    else
      flash[:errors] = @user.errors.full_messages
      redirect_to new_user_url
    end
  end

  def index
    @users = User.all
    render :index
  end

  def show
    @user = User.find(params[:id])
    render :show
  end

  # def update
#     @user = User.find(params[:id])
#
#   end

  # def destroy
#   end

end
