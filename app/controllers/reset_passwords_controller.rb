class ResetPasswordsController < ApplicationController

  def new
    render :new
  end

  def create
    @user = User.find_by_email(params[:user][:email])
    if @user.nil?
      flash[:errors] = ["No such user!"]
      redirect_to new_session_url
    else
      reset_token = @user.password_reset_token!
      msg = UserMailer.reset_email(@user)
      msg.deliver!
      flash[:notice] = "Password reset email"
      redirect_to new_session_url
    end
  end

  def new_pass
    @user = User.find(params[:user])
    if @user && @user.password_reset_token == params[:reset_token]
      render :new_pass
    else
      redirect_to new_user_url
    end
  end

  def update
    @user = User.find(params[:user][:id])
    @user.update_attributes(:password => params[:user][:password])
    if @user.save
      flash[:notice] = "Password successfully changed"
      @user.password_reset_token = nil
      redirect_to user_url(@user)
    else
      flash[:errors] = @user.errors.full_messages
      redirect_to new_session_url
    end

  end

end