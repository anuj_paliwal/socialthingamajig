class SessionsController < ApplicationController

  def new
    render :new
  end

  def create
    @user = User.find_by_credentials(params[:user][:email], params[:user][:password])
    if @user
      session[:session_token] = @user.reset_session_token!
      redirect_to user_url(@user)
    else
      flash[:errors] = ["You done messed up."]
      redirect_to new_session_url
    end
  end

  def destroy
    session[:session_token] = nil
    redirect_to users_url
  end

end
