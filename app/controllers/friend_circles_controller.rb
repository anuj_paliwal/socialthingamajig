class FriendCirclesController < ApplicationController
  def new
    @users = User.all
    render :new
  end

  def create
    @friend_circle = FriendCircle.new(params[:friend_circle])
    @friend_circle.member_ids = params[:friend_circle_membership][:friends]
    # @friend_circle.memberships.new(params[:friend_circle_membership].values)

    if @friend_circle.save
      flash[:notice] = "#{@friend_circle.name} circle created!"
      redirect_to friend_circles_url
    else
      flash[:errors] = @friend_circle.errors.full_messages
      redirect_to new_friend_circle_url
    end
  end

  def index
    @friend_circles = FriendCircle.all
    render :index
  end

  def show
    @friend_circle = FriendCircle.new(params[:id])
    render :show
  end

  # def update
  #
  # end

  # def destroy
  #
  # end

end
