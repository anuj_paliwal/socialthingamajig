class FriendCircleMembership < ActiveRecord::Base
  attr_accessible :user_id, :circle_id

  validates :user_id, :circle_id, presence: true

  belongs_to(
    :user,
    class_name: "User",
    foreign_key: :user_id,
    primary_key: :id
  )

  belongs_to(
    :circle,
    class_name: "FriendCircle",
    foreign_key: :circle_id,
    primary_key: :id
  )


end
