class User < ActiveRecord::Base
  attr_accessible :email, :session_token, :password_digest,
                  :password, :password_reset_token
  attr_reader :password

  validates :email, uniqueness: true
  validates :email, :session_token, :password_digest, presence: true
  validates :password, length: {minimum: 6}, allow_nil: true
  before_validation :ensure_session_token


  has_many(
    :memberships,
    class_name: "FriendCircleMembership",
    foreign_key: :user_id,
    primary_key: :id
  )

  has_many :circles, through: :memberships, :source => :circle

  def self.find_by_credentials(email, password)
    u = User.find_by_email(email)
    return u if u && u.is_password?(password)
    nil
  end

  def self.generate_session_token
    SecureRandom::urlsafe_base64(16)
  end

  def reset_session_token!
    self.session_token = self.class.generate_session_token
    self.save!
    self.session_token
  end

  def ensure_session_token
    self.session_token ||= self.class.generate_session_token
  end

  def password=(password)
    @password = password
    self.password_digest = BCrypt::Password.create(password)
  end

  def is_password?(password)
    BCrypt::Password.new(self.password_digest).is_password?(password)
  end


  def self.generate_password_reset_token
    SecureRandom::urlsafe_base64(16)
  end

  def password_reset_token!
    self.password_reset_token = self.class.generate_password_reset_token
    self.save!
    self.password_reset_token
  end

end
