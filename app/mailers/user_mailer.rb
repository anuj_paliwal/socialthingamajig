class UserMailer < ActionMailer::Base
  default from: "Anuj_Tim@localhost:3000"

  def reset_email(user)
    @user = user
    reset_token = @user.password_reset_token
    @url_reset = Addressable::URI.new(
       scheme: "http",
       host: "localhost:3000",
       path: "reset_password/new_pass",
       query_values: {
         user: @user.id,
         reset_token: reset_token
       }
    ).to_s
    mail(to: @user.email, subject: 'Password Reset')
  end



end
