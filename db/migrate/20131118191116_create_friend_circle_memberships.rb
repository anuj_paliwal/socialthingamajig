class CreateFriendCircleMemberships < ActiveRecord::Migration
  def change
    create_table :friend_circle_memberships do |t|
      t.integer :circle_id, null: false
      t.integer :user_id, null: false

      t.timestamps
    end

    add_index :friend_circle_memberships, :circle_id
    add_index :friend_circle_memberships, :user_id
  end
end
