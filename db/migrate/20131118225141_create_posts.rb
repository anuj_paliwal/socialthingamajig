class CreatePosts < ActiveRecord::Migration
  def change
    create_table :posts do |t|
      t.string   :description, null: false
      t.text     :body, null: false
      t.integer  :post_share_id, null: false

      t.timestamps
    end
  end
end
